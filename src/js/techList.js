$('#tech-list').slick({
  infinite: true,
  slidesToShow: 5,
  slidesToScroll: 2,
  autoplay: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});


//TOGGLE INGO FOR TECH LIST
$('#tech-list li').click( function(){
    $('#tech-list li').addClass('desaturate');
    $(this).removeClass('desaturate');
});
