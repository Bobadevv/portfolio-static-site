$(function () {
  var $root = $('html, body');
  $('.scroll-link').click(function() {
    $root.animate({
      scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
  });
});
